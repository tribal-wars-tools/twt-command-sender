/**
 * Initial version of the plugin by pboymt (MIT License).
 * https://github.com/pboymt/userscript-typescript-template
 */

import packageJsonFile from "../package.json";
import { packageJsonOptionsSchema } from "./types/userscript";

const packageJsonParsed = packageJsonOptionsSchema.parse(packageJsonFile);

/**
 * Generate a userscript's headers from "package.json" file.
 *
 * @returns Return userscript's header.
 */
export function generateHeader() {
  // The regular expression used to remove the dependency version string prefix.
  const dependencyVersionRegExp = /^[\^~]/;
  // Userscript's header.
  const headers = ["// ==UserScript=="];

  /**
   * Add userscript header's name.
   * If the name is not set, the package name is used. If neither is set, an error is thrown.
   */
  if (packageJsonParsed.name || packageJsonParsed.userscript.name) {
    headers.push(
      `// @name ${packageJsonParsed.userscript.name ?? packageJsonParsed.name}`
    );
  } else {
    throw new Error("No name specified in package.json");
  }
  /**
   * Add userscript header's version.
   * If the version is not set, the package version is used. If neither is set, an error is thrown.
   */
  if (packageJsonParsed.version || packageJsonParsed.userscript.version) {
    headers.push(
      `// @version ${
        packageJsonParsed.userscript.version ?? packageJsonParsed.version
      }`
    );
  } else {
    throw new Error("No version specified in package.json");
  }
  // Add userscript header's namespace.
  if (packageJsonParsed.userscript.namespace) {
    headers.push(`// @namespace ${packageJsonParsed.userscript.namespace}`);
  }
  // Add userscript header's description.
  if (
    packageJsonParsed.description ||
    packageJsonParsed.userscript.description
  ) {
    headers.push(
      `// @description ${
        packageJsonParsed.userscript.description ??
        packageJsonParsed.description
      }`
    );
  }
  // Add userscript header's author.
  if (packageJsonParsed.userscript.author) {
    headers.push(`// @author ${packageJsonParsed.userscript.author}`);
  } else if (packageJsonParsed.author) {
    headers.push(
      `// @author ${
        packageJsonParsed.author instanceof Object
          ? packageJsonParsed.author.name
          : packageJsonParsed.author
      }`
    );
  }
  // Add userscript header's homepage, homepageURL, website or source.
  if (packageJsonParsed.homepage || packageJsonParsed.userscript.homepage) {
    headers.push(
      `// @homepage ${
        packageJsonParsed.userscript.homepage ?? packageJsonParsed["homepage"]
      }`
    );
  } else if (packageJsonParsed.userscript.homepageURL) {
    headers.push(`// @homepageURL ${packageJsonParsed.userscript.homepageURL}`);
  } else if (packageJsonParsed.userscript.website) {
    headers.push(`// @website ${packageJsonParsed.userscript.website}`);
  } else if (packageJsonParsed.userscript.source) {
    headers.push(`// @source ${packageJsonParsed.userscript.source}`);
  }
  // Add userscript header's icon, iconURL or defaulticon.
  if (packageJsonParsed.userscript.icon) {
    headers.push(`// @icon ${packageJsonParsed.userscript.icon}`);
  } else if (packageJsonParsed.userscript.iconURL) {
    headers.push(`// @iconURL ${packageJsonParsed.userscript.iconURL}`);
  } else if (packageJsonParsed.userscript.defaulticon) {
    headers.push(`// @defaulticon ${packageJsonParsed.userscript.defaulticon}`);
  }
  // Add userscript header's icon64 or icon64URL.
  if (packageJsonParsed.userscript.icon64) {
    headers.push(`// @icon64 ${packageJsonParsed.userscript.icon64}`);
  } else if (packageJsonParsed.userscript.icon64URL) {
    headers.push(`// @icon64URL ${packageJsonParsed.userscript.icon64URL}`);
  }
  // Add userscript header's updateURL.
  if (packageJsonParsed.userscript.updateURL) {
    headers.push(`// @updateURL ${packageJsonParsed.userscript.updateURL}`);
  }
  // Add userscript header's downloadURL.
  if (packageJsonParsed.userscript.downloadURL) {
    headers.push(`// @downloadURL ${packageJsonParsed.userscript.downloadURL}`);
  }
  // Add userscript header's supportURL.
  if (packageJsonParsed.userscript.supportURL) {
    headers.push(`// @supportURL ${packageJsonParsed.userscript.supportURL}`);
  }
  // Add userscript header's includes.
  if (
    packageJsonParsed.userscript.include &&
    packageJsonParsed.userscript.include instanceof Array
  ) {
    for (const include of packageJsonParsed.userscript.include) {
      headers.push(`// @include ${include}`);
    }
  }
  // Add userscript header's matches.
  if (
    packageJsonParsed.userscript.match &&
    packageJsonParsed.userscript.match instanceof Array
  ) {
    for (const match of packageJsonParsed.userscript.match) {
      headers.push(`// @match ${match}`);
    }
  }
  // Add userscript header's excludes.
  if (
    packageJsonParsed.userscript.exclude &&
    packageJsonParsed.userscript.exclude instanceof Array
  ) {
    for (const exclude of packageJsonParsed.userscript.exclude) {
      headers.push(`// @exclude ${exclude}`);
    }
  }
  /**
   * Add userscript header's requires.
   * The package name and version will be obtained from the "dependencies" field,
   * and the jsdelivr link will be generated automatically.
   * You can also set the string template with the parameters "{dependencyName}" and "{dependencyVersion}"
   * in the "require-template" field of the "userscript" object in the "package.json" file.
   */
  if (packageJsonParsed.dependencies) {
    const urlTemplate =
      packageJsonParsed.userscript["require-template"] ??
      "https://cdn.jsdelivr.net/npm/{dependencyName}@{dependencyVersion}";
    const requireTemplate = `// @require ${urlTemplate}`;
    for (const [name, value] of Object.entries(
      packageJsonParsed.dependencies
    )) {
      const dependencyVersion = value.replace(dependencyVersionRegExp, "");
      headers.push(
        requireTemplate
          .replace("{dependencyName}", name)
          .replace("{dependencyVersion}", dependencyVersion)
      );
    }
  }
  // You can also add dependencies separately in the require field of the userscript object.
  if (
    packageJsonParsed.userscript.require &&
    packageJsonParsed.userscript.require instanceof Array
  ) {
    for (const require of packageJsonParsed.userscript.require) {
      headers.push(`// @require ${require}`);
    }
  }
  // Add userscript header's resources.
  if (
    packageJsonParsed.userscript.resources &&
    packageJsonParsed.userscript.resources instanceof Array
  ) {
    for (const resource of packageJsonParsed.userscript.resources) {
      headers.push(`// @resource ${resource}`);
    }
  }
  // Add userscript header's connects.
  if (
    packageJsonParsed.userscript.connect &&
    packageJsonParsed.userscript.connect instanceof Array
  ) {
    for (const connect of packageJsonParsed.userscript.connect) {
      headers.push(`// @connect ${connect}`);
    }
  }
  // Add userscript header's run-at.
  if (packageJsonParsed.userscript["run-at"]) {
    headers.push(`// @run-at ${packageJsonParsed.userscript["run-at"]}`);
  }
  // Add userscript header's grants.
  if (
    packageJsonParsed.userscript.grant &&
    packageJsonParsed.userscript.grant instanceof Array
  ) {
    for (const grant of packageJsonParsed.userscript.grant) {
      headers.push(`// @grant ${grant}`);
    }
  }
  // Add userscript header's antifeatures.
  if (
    packageJsonParsed.userscript.antifeature &&
    packageJsonParsed.userscript.antifeature instanceof Array
  ) {
    for (const antifeature of packageJsonParsed.userscript.antifeature) {
      headers.push(`// @antifeature ${antifeature}`);
    }
  }
  // Add userscript header's noframes.
  if (packageJsonParsed.userscript.noframes) {
    headers.push("// @noframes");
  }
  // Add userscript header's nocompat.
  if (packageJsonParsed.userscript.nocompat) {
    headers.push(`// @nocompat ${packageJsonParsed.userscript.nocompat}`);
  }
  // Userscript header's ending.
  headers.push("// ==/UserScript==\n");
  return headers.join("\n");
}
