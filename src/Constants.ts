export const defaultPrefix = "command_sender";
export const defaultOffset = "-250";

export const iconsCSSHref =
  "https://cdn.jsdelivr.net/npm/@tabler/icons-webfont@latest/dist/tabler-icons.min.css";

export const footerNameTextContent = "twt-command-sender";
export const footerUrlTextContent = "codeberg.org/szelbi";
export const footerUrlHref = "https://codeberg.org/szelbi";
