# Tribal Wars Tools: Command sender

Automatic command sender for the game Tribal Wars.
You specify the arrival time or the sending time of a command, as well as the offset. An attack will be sent automatically.

The script is written in TypeScript and compiled to JavaScript (ES6) with appropriate UserScript headers using Webpack.

## Table of Contents

- [Installation](#installation)
- [Translations](#translations)
- [Plans for the future](#plans-for-the-future)

## Installation

Tutorial: [UserScipt Installation tutorial](https://codeberg.org/szelbi/userscipt-installation-tutorial)

You can find the script file here: [twt-command-sender.user.js](https://codeberg.org/tribal-wars-tools/twt-command-sender/raw/branch/main/userscript/twt-command-sender.user.js)

## Translations

Currently the script supports only the English language.

## Plans for the future

1. Add possibility to change the attack arrival time after the confirmation.
2. Add multiple language versions.

Do you have an idea for improving the extension? Share your suggestion here: [Issues](https://codeberg.org/tribal-wars-tools/twt-command-sender/issues)
