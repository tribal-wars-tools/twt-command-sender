// ==UserScript==
// @name twt-command-sender
// @version 1.2.1
// @description Tribal Wars Tools: Command sender. Userscript that allows you to specify the arrival time or the sending time of a command and send it automatically.
// @author szelbi
// @homepage https://codeberg.org/tribal-wars-tools/twt-command-sender
// @updateURL https://codeberg.org/tribal-wars-tools/twt-command-sender/raw/branch/main/userscript/twt-command-sender.user.js
// @downloadURL https://codeberg.org/tribal-wars-tools/twt-command-sender/raw/branch/main/userscript/twt-command-sender.user.js
// @match *://*/*
// @require https://cdn.jsdelivr.net/npm/luxon@3.4.4
// @grant unsafeWindow
// ==/UserScript==

/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ([
/* 0 */
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
const Constants = __importStar(__webpack_require__(1));
const luxon_1 = __webpack_require__(2);
const CommandSender = (function (prefix) {
    const determinedPrefix = prefix ?? Constants.defaultPrefix;
    let _timeTypeDiv = undefined;
    let _dateTimeInput = undefined;
    let _offsetInput = undefined;
    let _secondsInput = undefined;
    let _millisecondsInput = undefined;
    let _confirmButton = undefined;
    let _timeEditIcon = undefined;
    let _timeEditDiv = undefined;
    let _mainDiv = undefined;
    let _serverDateTimeDifference = undefined;
    function getTimeTypeGroupName() {
        return `${determinedPrefix}_time_type`;
    }
    function generateTimeTypeDiv() {
        const timeTypeGroupName = getTimeTypeGroupName();
        const timeTypeDiv = document.createElement("div");
        timeTypeDiv.textContent = "Which time do you want to specify?";
        timeTypeDiv.append(document.createElement("br"));
        const arrivalTimeRadio = document.createElement("input");
        arrivalTimeRadio.type = "radio";
        arrivalTimeRadio.name = timeTypeGroupName;
        arrivalTimeRadio.id = `${timeTypeGroupName}_arrival`;
        arrivalTimeRadio.checked = true;
        const arrivalTimeLabel = document.createElement("label");
        arrivalTimeLabel.htmlFor = arrivalTimeRadio.id;
        arrivalTimeLabel.textContent = "Arrival time";
        timeTypeDiv.append(arrivalTimeRadio);
        timeTypeDiv.append(arrivalTimeLabel);
        const sendTimeRadio = document.createElement("input");
        sendTimeRadio.type = "radio";
        sendTimeRadio.name = timeTypeGroupName;
        sendTimeRadio.id = `${timeTypeGroupName}_send`;
        const sendTimeLabel = document.createElement("label");
        sendTimeLabel.htmlFor = sendTimeRadio.id;
        sendTimeLabel.textContent = "Send time";
        timeTypeDiv.append(sendTimeRadio);
        timeTypeDiv.append(sendTimeLabel);
        return timeTypeDiv;
    }
    function getTimeTypeDiv() {
        if (!_timeTypeDiv) {
            _timeTypeDiv = generateTimeTypeDiv();
        }
        return _timeTypeDiv;
    }
    function generateDateTimeInput() {
        const datetimeInputId = `${determinedPrefix}_datetime`;
        const generatedDatetimeInput = document.createElement("input");
        generatedDatetimeInput.id = datetimeInputId;
        generatedDatetimeInput.type = "datetime-local";
        generatedDatetimeInput.step = ".001";
        setCurrentDateTime(generatedDatetimeInput);
        return generatedDatetimeInput;
    }
    function getDateTimeInput() {
        if (!_dateTimeInput) {
            _dateTimeInput = generateDateTimeInput();
        }
        return _dateTimeInput;
    }
    function generateTimeEditIcon() {
        const timeEditIconId = `${determinedPrefix}_time_edit_icon`;
        injectIconsCSS();
        const generatedTimeEditIcon = document.createElement("i");
        generatedTimeEditIcon.id = timeEditIconId;
        generatedTimeEditIcon.classList.add("ti");
        generatedTimeEditIcon.classList.add("ti-clock-edit");
        generatedTimeEditIcon.style.cursor = "pointer";
        generatedTimeEditIcon.title = "Split Time Editing";
        return generatedTimeEditIcon;
    }
    function getTimeEditIcon() {
        if (!_timeEditIcon) {
            _timeEditIcon = generateTimeEditIcon();
        }
        return _timeEditIcon;
    }
    function generateOffsetInput() {
        const offsetInputId = `${determinedPrefix}_offset`;
        const generatedOffsetInput = document.createElement("input");
        generatedOffsetInput.id = offsetInputId;
        generatedOffsetInput.type = "number";
        setDefaultOffset(generatedOffsetInput);
        return generatedOffsetInput;
    }
    function getOffsetInput() {
        if (!_offsetInput) {
            _offsetInput = generateOffsetInput();
        }
        return _offsetInput;
    }
    function generateSecondsInput() {
        const secondsInputId = `${determinedPrefix}_seconds`;
        const generatedSecondsInput = document.createElement("input");
        generatedSecondsInput.id = secondsInputId;
        generatedSecondsInput.type = "number";
        return generatedSecondsInput;
    }
    function getSecondsInput() {
        if (!_secondsInput) {
            _secondsInput = generateSecondsInput();
        }
        return _secondsInput;
    }
    function generateMillisecondsInput() {
        const millisecondsInputId = `${determinedPrefix}_milliseconds`;
        const generatedMillisecondsInput = document.createElement("input");
        generatedMillisecondsInput.id = millisecondsInputId;
        generatedMillisecondsInput.type = "number";
        return generatedMillisecondsInput;
    }
    function getMillisecondsInput() {
        if (!_millisecondsInput) {
            _millisecondsInput = generateMillisecondsInput();
        }
        return _millisecondsInput;
    }
    function generateTimeEditDiv() {
        const timeEditDivId = `${determinedPrefix}_time_edit`;
        const timeEditDiv = document.createElement("div");
        timeEditDiv.id = timeEditDivId;
        timeEditDiv.style.border = "2px dotted black";
        timeEditDiv.style.padding = "5px";
        timeEditDiv.style.display = "none";
        const secondsInput = getSecondsInput();
        const secondsInputId = secondsInput.id;
        if (secondsInputId) {
            const secondsLabel = document.createElement("label");
            secondsLabel.htmlFor = secondsInputId;
            secondsLabel.textContent = "Seconds: ";
            timeEditDiv.append(secondsLabel);
        }
        timeEditDiv.append(secondsInput);
        timeEditDiv.append(document.createElement("br"));
        const millisecondsInput = getMillisecondsInput();
        const millisecondsInputId = millisecondsInput.id;
        if (millisecondsInputId) {
            const millisecondsLabel = document.createElement("label");
            millisecondsLabel.htmlFor = millisecondsInputId;
            millisecondsLabel.textContent = "Milliseconds: ";
            timeEditDiv.append(millisecondsLabel);
        }
        timeEditDiv.append(millisecondsInput);
        timeEditDiv.append(document.createElement("br"));
        return timeEditDiv;
    }
    function getTimeEditDiv() {
        if (!_timeEditDiv) {
            _timeEditDiv = generateTimeEditDiv();
        }
        return _timeEditDiv;
    }
    function generateConfirmButton() {
        const confirmButtonId = `${determinedPrefix}_confirm`;
        const generatedConfirmButton = document.createElement("input");
        generatedConfirmButton.id = confirmButtonId;
        generatedConfirmButton.type = "button";
        generatedConfirmButton.value = "Confirm";
        return generatedConfirmButton;
    }
    function getConfirmButton() {
        if (!_confirmButton) {
            _confirmButton = generateConfirmButton();
        }
        return _confirmButton;
    }
    function generateMainDiv() {
        const mainDivId = `${determinedPrefix}_main`;
        const mainDiv = document.createElement("div");
        mainDiv.id = mainDivId;
        const dateTimeInput = getDateTimeInput();
        const datetimeInputId = dateTimeInput.id;
        if (datetimeInputId) {
            const datetimeLabel = document.createElement("label");
            datetimeLabel.htmlFor = datetimeInputId;
            datetimeLabel.textContent = "Time: ";
            mainDiv.append(datetimeLabel);
        }
        mainDiv.append(dateTimeInput);
        const timeEditIcon = getTimeEditIcon();
        timeEditIcon.addEventListener("click", onTimeEditIconClick);
        mainDiv.append(timeEditIcon);
        mainDiv.append(document.createElement("br"));
        const timeEditDiv = getTimeEditDiv();
        mainDiv.append(timeEditDiv);
        mainDiv.append(document.createElement("br"));
        const offsetInput = getOffsetInput();
        const offsetInputId = offsetInput.id;
        if (offsetInputId) {
            const offsetLabel = document.createElement("label");
            offsetLabel.htmlFor = offsetInputId;
            offsetLabel.textContent = "Offset (ms): ";
            mainDiv.append(offsetLabel);
        }
        mainDiv.append(offsetInput);
        mainDiv.append(document.createElement("br"));
        const confirmButton = getConfirmButton();
        confirmButton.addEventListener("click", onConfirmButtonClick);
        mainDiv.append(confirmButton);
        return mainDiv;
    }
    function getMainDiv() {
        if (!_mainDiv) {
            _mainDiv = generateMainDiv();
        }
        return _mainDiv;
    }
    function getCommandDataForm() {
        const commandDataForm = document.querySelector("form[id='command-data-form']");
        return commandDataForm ?? undefined;
    }
    function getConfirmAttackButton() {
        const commandDataForm = getCommandDataForm();
        if (!commandDataForm) {
            throw new Error(`${determinedPrefix}: Could not get CommandData form.`);
        }
        const confirmAttackButton = commandDataForm.querySelector("#troop_confirm_submit");
        return confirmAttackButton ?? undefined;
    }
    function onTimeEditIconClick() {
        const timeEditDiv = getTimeEditDiv();
        toggleDivVisibility(timeEditDiv);
        try {
            if (timeEditDiv.style.display === "none") {
                mergeDateTime();
                return;
            }
            splitDateTime();
        }
        catch (error) {
            console.error(error);
            return;
        }
    }
    function getSplitDateTime() {
        const dateTime = getMergedDateTime();
        const secondsInput = getSecondsInput();
        const seconds = Number.parseInt(secondsInput.value);
        if (Number.isNaN(seconds)) {
            throw new Error(`${determinedPrefix}: Could not parse integer from seconds input.`);
        }
        const millisecondsInput = getMillisecondsInput();
        const milliseconds = Number.parseInt(millisecondsInput.value);
        if (Number.isNaN(milliseconds)) {
            throw new Error(`${determinedPrefix}: Could not parse integer from milliseconds input.`);
        }
        const mergedDateTime = dateTime.set({
            second: seconds,
            millisecond: milliseconds,
        });
        return mergedDateTime;
    }
    function getMergedDateTime() {
        const dateTimeInput = getDateTimeInput();
        const dateTime = luxon_1.DateTime.fromISO(dateTimeInput.value);
        if (!dateTime.isValid) {
            throw new Error(`${determinedPrefix}: Could not parse DateTime from DateTime input.`);
        }
        return dateTime;
    }
    function getDateTime() {
        const timeEditDiv = getTimeEditDiv();
        if (timeEditDiv.style.display === "none") {
            return getMergedDateTime();
        }
        return getSplitDateTime();
    }
    function splitDateTime() {
        const dateTime = getMergedDateTime();
        const secondsInput = getSecondsInput();
        secondsInput.value = `${dateTime.second}`;
        const millisecondsInput = getMillisecondsInput();
        millisecondsInput.value = `${dateTime.millisecond}`;
        const newDateTime = dateTime.set({ second: 0, millisecond: 0 });
        const isoDateTimeString = newDateTime.toISO({ includeOffset: false });
        const dateTimeInput = getDateTimeInput();
        dateTimeInput.value = isoDateTimeString;
        dateTimeInput.step = "60"; // Change input step to 60 seconds.
    }
    function mergeDateTime() {
        const dateTimeInput = getDateTimeInput();
        dateTimeInput.step = ".001"; // Change input step to milliseconds.
        const dateTime = getSplitDateTime();
        const isoDateTimeString = dateTime.toISO({ includeOffset: false });
        dateTimeInput.value = isoDateTimeString;
        const secondsInput = getSecondsInput();
        secondsInput.value = "";
        const millisecondsInput = getMillisecondsInput();
        millisecondsInput.value = "";
    }
    function onConfirmButtonClick() {
        try {
            const offsetInput = getOffsetInput();
            const offset = Number.parseInt(offsetInput.value);
            if (Number.isNaN(offset)) {
                throw new Error(`${determinedPrefix}: Could not parse integer from offset input.`);
            }
            localStorage.setItem(offsetInput.id, `${offset}`);
            const confirmAttackButton = getConfirmAttackButton();
            if (!confirmAttackButton) {
                throw new Error(`${determinedPrefix}: Could not get attack confirm button.`);
            }
            confirmAttackButton.classList.add("btn-disabled");
            const serverDateTime = getServerDateTime();
            const serverDateTimeDifference = getServerDateTimeDifference();
            const realServerDateTime = serverDateTime.plus(serverDateTimeDifference);
            const dateTime = getDateTime();
            let timeoutDuration = dateTime.diff(realServerDateTime);
            const offsetDuration = luxon_1.Duration.fromMillis(offset);
            timeoutDuration = timeoutDuration.plus(offsetDuration);
            const timeTypeDiv = getTimeTypeDiv();
            const checkedRadio = timeTypeDiv.querySelector("input[type='radio']:checked");
            if (!checkedRadio) {
                throw new Error(`${determinedPrefix}: Could not get checked radio button.`);
            }
            const timeTypeGroupName = getTimeTypeGroupName();
            const commandDataForm = getCommandDataForm();
            if (!commandDataForm) {
                throw new Error(`${determinedPrefix}: Could not get CommandData form.`);
            }
            if (checkedRadio.id === `${timeTypeGroupName}_arrival`) {
                const dateArrivalTd = commandDataForm.querySelector("td[id='date_arrival']");
                if (!dateArrivalTd) {
                    throw new Error(`${determinedPrefix}: Could not get arrival date data cell.`);
                }
                const closestTbody = dateArrivalTd.closest("tbody");
                if (!closestTbody) {
                    throw new Error(`${determinedPrefix}: Could not find any tbody ancestor of arrival date data cell.`);
                }
                const commandTableTds = closestTbody.querySelectorAll("td");
                if (!commandTableTds) {
                    throw new Error(`${determinedPrefix}: Could not get command table data cells.`);
                }
                const durationRegex = /^\d+\:\d{1,2}\:\d{1,2}$/;
                const durationMatch = Array.from(commandTableTds).find((td) => durationRegex.test(td.textContent ?? ""));
                if (!durationMatch) {
                    throw new Error(`${determinedPrefix}: Could not find any match for duration regex.`);
                }
                const attackDurationString = durationMatch.textContent;
                if (!attackDurationString) {
                    throw new Error(`${determinedPrefix}: Could not find the contents of the duration table cell.`);
                }
                const attackDurationDateTime = luxon_1.DateTime.fromFormat(attackDurationString, "H:mm:ss");
                if (!attackDurationDateTime.isValid) {
                    throw new Error(`${determinedPrefix}: The attack duration DateTime object created from string is invalid.`);
                }
                const attackDuration = luxon_1.Duration.fromObject({
                    hours: attackDurationDateTime.hour,
                    minutes: attackDurationDateTime.minute,
                    seconds: attackDurationDateTime.second,
                });
                const attackDurationMilliseconds = attackDuration.toMillis();
                timeoutDuration = timeoutDuration.minus(attackDurationMilliseconds);
            }
            const timeoutDurationMilliseconds = timeoutDuration.toMillis();
            setTimeout(() => {
                if (!(confirmAttackButton instanceof HTMLElement)) {
                    throw new Error(`${determinedPrefix}: Could not click on the attack confirm button because it is not an HTML element.`);
                }
                confirmAttackButton.click();
            }, timeoutDurationMilliseconds);
            const confirmButton = getConfirmButton();
            confirmButton.disabled = true;
        }
        catch (error) {
            console.error(error);
            return;
        }
    }
    function isObject(value) {
        if (typeof value !== "object") {
            return false;
        }
        if (value === null) {
            return false;
        }
        return true;
    }
    function getServerDateTime() {
        if (!isObject(unsafeWindow)) {
            throw new Error(`${determinedPrefix}: The unsafeWindow global property is not an object.`);
        }
        if (!("Timing" in unsafeWindow)) {
            throw new Error(`${determinedPrefix}: The Timing property does not exist on window object.`);
        }
        if (typeof unsafeWindow.Timing !== "object" ||
            unsafeWindow.Timing === null) {
            throw new Error(`${determinedPrefix}: The Timing property is not an object.`);
        }
        if (!("getCurrentServerTime" in unsafeWindow.Timing)) {
            throw new Error(`${determinedPrefix}: The getCurrentServerTime property does not exist on window object.`);
        }
        if (typeof unsafeWindow.Timing.getCurrentServerTime !== "function") {
            throw new Error(`${determinedPrefix}: The getCurrentServerTime property is not a function.`);
        }
        const serverMilliseconds = unsafeWindow.Timing.getCurrentServerTime();
        if (typeof serverMilliseconds !== "number") {
            throw new Error(`${determinedPrefix}: The server time was returned in a format other than a number.`);
        }
        const serverDateTime = luxon_1.DateTime.fromMillis(serverMilliseconds);
        if (!serverDateTime.isValid) {
            throw new Error(`${determinedPrefix}: The server DateTime object created from milliseconds is invalid.`);
        }
        return serverDateTime;
    }
    function getServerDateTimeDisplayed() {
        const serverDateSpan = document.getElementById("serverDate");
        if (!serverDateSpan) {
            throw new Error(`${determinedPrefix}: The server date HTML element could not be found.`);
        }
        const serverDateDisplayed = luxon_1.DateTime.fromFormat(serverDateSpan.innerText, "dd/MM/yyyy");
        if (!serverDateDisplayed.isValid) {
            throw new Error(`${determinedPrefix}: The server DateTime object created from date string displayed is invalid.`);
        }
        if (!isObject(unsafeWindow)) {
            throw new Error(`${determinedPrefix}: The unsafeWindow global property is not an object.`);
        }
        if (!("server_utc_diff" in unsafeWindow)) {
            throw new Error(`${determinedPrefix}: The server_utc_diff property does not exist on window object.`);
        }
        if (typeof unsafeWindow.server_utc_diff !== "number") {
            throw new Error(`${determinedPrefix}: The server UTC difference was returned in a format other than a number.`);
        }
        const serverUTCDiff = unsafeWindow.server_utc_diff;
        const serverDateTime = getServerDateTime();
        const serverDateTimeSeconds = serverDateTime.toSeconds();
        const serverDateTimeSecondsCorrected = serverDateTimeSeconds + serverUTCDiff;
        if (!("getTimeString" in unsafeWindow)) {
            throw new Error(`${determinedPrefix}: The getTimeString property does not exist on window object.`);
        }
        if (typeof unsafeWindow.getTimeString !== "function") {
            throw new Error(`${determinedPrefix}: The getTimeString property is not a function.`);
        }
        const serverTimeDisplayedString = unsafeWindow.getTimeString(serverDateTimeSecondsCorrected, true, true);
        const serverTimeDisplayed = luxon_1.DateTime.fromISO(serverTimeDisplayedString);
        if (!serverTimeDisplayed.isValid) {
            throw new Error(`${determinedPrefix}: The server DateTime object created from time string displayed is invalid.`);
        }
        const serverDateTimeDisplayed = serverTimeDisplayed.set({
            year: serverDateDisplayed.year,
            month: serverDateDisplayed.month,
            day: serverDateDisplayed.day,
        });
        if (!serverTimeDisplayed.isValid) {
            throw new Error(`${determinedPrefix}: The server DateTime object created from date & time objects is invalid.`);
        }
        return serverDateTimeDisplayed;
    }
    function calculateServerDateTimeDifference() {
        const serverDateTime = getServerDateTime();
        const serverDateTimeMinutesAccuracy = serverDateTime.set({
            second: 0,
            millisecond: 0,
        });
        const serverDateTimeDisplayed = getServerDateTimeDisplayed();
        const serverDateTimeDisplayedMinutesAccuracy = serverDateTimeDisplayed.set({
            second: 0,
            millisecond: 0,
        });
        const serverDateTimeMinutesAccuracyMilliseconds = serverDateTimeMinutesAccuracy.toMillis();
        const serverDateTimeDifference = serverDateTimeDisplayedMinutesAccuracy.minus(serverDateTimeMinutesAccuracyMilliseconds);
        const serverDateTimeDifferenceMilliseconds = serverDateTimeDifference.toMillis();
        return luxon_1.Duration.fromMillis(serverDateTimeDifferenceMilliseconds);
    }
    function getServerDateTimeDifference() {
        if (!_serverDateTimeDifference) {
            _serverDateTimeDifference = calculateServerDateTimeDifference();
        }
        return _serverDateTimeDifference;
    }
    function setCurrentDateTime(dateTimeInput) {
        // For the DateTime input we want to use the local DateTime, so it's the same as on the user's device.
        const nowDateTime = luxon_1.DateTime.now();
        const isoDateTimeString = nowDateTime.toISO({ includeOffset: false });
        dateTimeInput.value = isoDateTimeString;
    }
    function setDefaultOffset(offsetInput) {
        const offsetInputId = offsetInput.id;
        if (!offsetInputId) {
            return;
        }
        const offsetValue = localStorage.getItem(offsetInputId) || Constants.defaultOffset;
        offsetInput.value = offsetValue;
    }
    function injectIconsCSS() {
        if (isStyleSheetLoaded(Constants.iconsCSSHref)) {
            return;
        }
        const iconsCSSLink = document.createElement("link");
        iconsCSSLink.rel = "stylesheet";
        iconsCSSLink.href = Constants.iconsCSSHref;
        document.head.appendChild(iconsCSSLink);
    }
    function isStyleSheetLoaded(href) {
        for (const styleSheet of document.styleSheets) {
            if (styleSheet.href === href) {
                return true;
            }
        }
        return false;
    }
    function toggleDivVisibility(div) {
        div.style.display = div.style.display === "none" ? "inline-block" : "none";
    }
    return {
        init: () => {
            const commandDataForm = getCommandDataForm();
            if (!commandDataForm) {
                console.error(`${determinedPrefix}: Could not get CommandData form.`);
                return;
            }
            const mainDiv = getMainDiv();
            commandDataForm.prepend(mainDiv);
            const timeTypeDiv = getTimeTypeDiv();
            mainDiv.prepend(timeTypeDiv);
        },
        addFooter: () => {
            const serverInfoParagraph = document.querySelector("p[class='server_info']");
            const footerSpan = document.createElement("span");
            footerSpan.style.cssFloat = "left";
            const footerName = document.createElement("b");
            footerName.textContent = Constants.footerNameTextContent;
            const footerUrl = document.createElement("a");
            footerUrl.textContent = Constants.footerUrlTextContent;
            footerUrl.href = Constants.footerUrlHref;
            footerUrl.target = "_blank";
            footerSpan.innerHTML = `${footerName.outerHTML} by szelbi (${footerUrl.outerHTML})`;
            serverInfoParagraph?.prepend(footerSpan);
        },
    };
})();
const url = window.location.href;
const urlRegex = /^.*:\/\/.*\/game\.php.*screen=place.*try=confirm.*$/;
if (urlRegex.test(url)) {
    CommandSender.init();
    CommandSender.addFooter();
}


/***/ }),
/* 1 */
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.footerUrlHref = exports.footerUrlTextContent = exports.footerNameTextContent = exports.iconsCSSHref = exports.defaultOffset = exports.defaultPrefix = void 0;
exports.defaultPrefix = "command_sender";
exports.defaultOffset = "-250";
exports.iconsCSSHref = "https://cdn.jsdelivr.net/npm/@tabler/icons-webfont@latest/dist/tabler-icons.min.css";
exports.footerNameTextContent = "twt-command-sender";
exports.footerUrlTextContent = "codeberg.org/szelbi";
exports.footerUrlHref = "https://codeberg.org/szelbi";


/***/ }),
/* 2 */
/***/ ((module) => {

module.exports = luxon;

/***/ })
/******/ 	]);
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__(0);
/******/ 	
/******/ })()
;